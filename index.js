let user;
let pw;
let role;



function login() {
	user = prompt("Enter your username: ");
	if (!user) {
		alert("The username should not be empty!");
		throw "The username should not be empty!";
		return;
	}
	pw = prompt("Enter your password: ");
	if (!pw) {
		alert("The password should not be empty!");
		throw "The password should not be empty!";
		return;
	}
	role = prompt("Enter your role: ");

	role = role.toLowerCase();




	if (!role || role === "admin" || role === "teacher") {
		alert(`${role}! You are not allowed to access this feature!`);
		throw `${role}! You are not allowed to access this feature!`;
	} else {
		switch(role) {
			case "admin":
				alert("Welcome to class admin!");
				break;
			case "student":
				alert("Welcome to class student!");
				break;
			default:
				alert("Role out of range.");
				throw `${role}! You are not allowed to access this feature!`;
				return;
		}

	}

}



 try {
 	login();
 	function checkGrades(num1,num2,num3,num4) {
	let average = (num1 + num2 + num3 + num4) / 4;

	average = Math.round(average);

	console.log(average);

		if (average <= 74) {
			console.log(`Hello, student, your average is ${average}. The letter equivalent is F`);
		} else if (average <= 79) {
			console.log(`Hello, student, your average is ${average}. The letter equivalent is D`);
		} else if (average <= 84) {
			console.log(`Hello, student, your average is ${average}. The letter equivalent is C`);
		} else if (average <= 89) {
			console.log(`Hello, student, your average is ${average}. The letter equivalent is B`);
		} else if (average <= 95) {
			console.log(`Hello, student, your average is ${average}. The letter equivalent is A`);
		} else if (average <= 100){
			console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`);
		} else {
			console.log("SHEESH!");
		}
	}
 }

 catch(error) {
 	console.log(`Error message: ${error}`);
 	throw 'Stopping the script so student grades are not accessed'; 
 }


 checkGrades(50,50,50,50);






/*
    Code Guide:
    
    // [Function without return]
    function printName(name){ 
	  console.log(name);
    }

    printName("Juana"); 
    
     // [Function with return]
    function displayName(name){ 
      return name;
    }
    let sampleName = displayName("Julie"); 
    console.log(sampleName);


	// [Function without return]
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
    
    // [Function with return]
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
 	
    // [Function with Return]
	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
    
    // [Function with Return]
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	
    // [Function with Return]
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
